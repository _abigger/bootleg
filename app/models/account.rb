class Account < ActiveRecord::Base
  validates :name, presence: true
  has_many :promo_code, dependent: :destroy
  accepts_nested_attributes_for(
    :promo_code,
    allow_destroy: true,
    reject_if: ->(promo_code) { promo_code[:product_id].blank? }
  )
end
