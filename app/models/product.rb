class Product < ActiveRecord::Base
  validates :name, presence: true
  has_many :promo_code, dependent: :destroy
  has_attached_file :file
end
