class PromoCode < ActiveRecord::Base
  has_one :product
  before_save :set_code

  def set_code
    self.code ||= SecureRandom.hex.split(//).last(5).join('').to_s
  end
end
