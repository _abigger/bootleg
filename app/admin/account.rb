ActiveAdmin.register Account do
  permit_params :name, promo_code_attributes: %i[id product_id quota download_count _destroy]

  index do
    selectable_column
    column :name
    actions
  end

  form do |f|
    f.inputs 'Account Details' do
      f.input :name
    end
    panel 'Promo Codes' do
      f.has_many :promo_code, heading: '', allow_destroy: true, new_record: true do |pc|
        pc.input(
          :product_id,
          as: :select,
          collection: Product.all.map { |p| [p.name, p.id] },
          include_blank: false
        )
        pc.input :quota
        pc.input :download_count
        pc.actions
      end
      f.actions
    end
  end

  show do
    panel 'Account Details' do
      attributes_table_for account do
        row('Account Id') { account.id }
        row('Name')       { account.name }
        row('Created At') { account.created_at }
      end
    end
    panel 'Promo Codes' do
      account.promo_code.each do |promo_code|
        table_for promo_code do
          column(:product_name) { Product.find(promo_code.product_id).name }
          column(:code)
          column(:quota)
          column(:download_count)
        end
      end
    end
  end
end
