ActiveAdmin.register Product do
  permit_params :name, :file

  index do
    selectable_column
    column :name
    column :file, sortable: :file do |product|
      product.file.exists? ? status_tag('Available', :ok) : status_tag('Not Available')
    end
    actions
  end

  form do |f|
    f.inputs 'Product' do
      f.input :name
      f.input :file, as: :file
    end
    f.actions
  end

  show do |product|
    attributes_table do
      row :name
      row :file do
        product.file.exists? ? status_tag('Available', :ok) : status_tag('Not Available')
      end
    end
  end
end
