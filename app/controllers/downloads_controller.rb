class DownloadsController < ApplicationController
  include DownloadRules
  include DownloadActions
  include DownloadExceptions

  def index
    render 'index'
  end

  def show
    @promo_code ||= PromoCode.find_by_code(params[:download][:promo_code])
    check_promo_code
    check_quota
    update_download_count
    send_file
  end

  private

  def download_params
    params.require(:download).permit(:promo_code)
  end
end
