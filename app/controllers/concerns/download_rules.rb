module DownloadRules
  extend ActiveSupport::Concern

  def check_promo_code
    return unless @promo_code.nil?
    raise PromoCodeNotFound
  end

  def check_quota
    return unless @promo_code.download_count >= @promo_code.quota
    raise DownloadExceedsQuota
  end
end
