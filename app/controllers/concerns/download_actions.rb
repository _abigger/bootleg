module DownloadActions
  extend ActiveSupport::Concern

  def update_download_count
    @promo_code.download_count += 1
    @promo_code.save
  end

  def send_file
    product = Product.find_by_id(@promo_code.product_id)
    send_file product.file.path, type: product.file_content_type
  end
end
