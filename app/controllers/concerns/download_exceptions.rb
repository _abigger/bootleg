module DownloadExceptions
  extend ActiveSupport::Concern

  included do
    rescue_from(StandardError) { something_went_wrong }
    rescue_from(PromoCodeNotFound)    { promo_code_not_found }
    rescue_from(DownloadExceedsQuota) { download_exceeds_quota }
  end

  def something_went_wrong
    redirect_to(
      root_path,
      flash: {
        error: I18n.t('notices.something_went_wrong')
      }
    )
  end

  def promo_code_not_found
    redirect_to(
      root_path,
      flash: {
        warning: I18n.t('notices.incorrect_promo_code')
      }
    )
  end

  def download_exceeds_quota
    redirect_to(
      root_path,
      flash: {
        error: I18n.t('notices.download_exceeds_quota')
      }
    )
  end
end
