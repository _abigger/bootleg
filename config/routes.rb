Rails.application.routes.draw do
  root to: "downloads#index"

  resources :downloads, only: [:index, :show]

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
end
