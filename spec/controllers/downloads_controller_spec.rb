require 'rails_helper'

RSpec.describe DownloadsController, type: :controller do
  let(:valid_session) { {} }

  describe 'GET #index' do
    it 'renders index page' do
      get :index, {}, valid_session
      expect(response).to render_template('index')
    end
  end

  describe 'GET #show' do
    let(:show_params) do
      {
        'download' => {
          'promo_code' => promo_code
        },
        'commit' => 'Go',
        'id' => 'show'
      }
    end

    describe 'invalid promo code' do
      let(:promo_code) { 'a848474' }

      before do
        get :show, show_params, valid_session
      end

      it 'redirects to root page' do
        expect(response).to redirect_to(root_path)
      end

      it 'sets incorrect book warning flash message' do
        expect(controller).to set_flash[:warning]
          .to('Incorrect book code, please try again.')
      end
    end

    describe 'valid promo code' do
      let(:product_params) do
        {
          name: 'Some Product',
          file_file_name: 'product-file-name'
        }
      end
      let(:product_object) { Product.new(product_params) }
      let(:promo_params) do
        {
          quota: quota,
          download_count: download_count,
          code: promo_code,
          product_id: 1,
          account_id: 1
        }
      end
      let(:promo_code_object) { PromoCode.new(promo_params) }

      before do
        allow(Product).to receive(:find_by_id)
          .and_return(product_object)

        allow(PromoCode).to receive(:find_by_code)
          .with(promo_code)
          .and_return(promo_code_object)
      end

      describe 'when download count exceeds quota' do
        let(:quota) { 1 }
        let(:download_count) { 1 }
        let(:promo_code) { 'b848478' }

        before do
          get :show, show_params, valid_session
        end

        it 'redirects to root page' do
          expect(response).to redirect_to(root_path)
        end

        it 'sets incorrect book warning flash message' do
          expect(controller).to set_flash[:error]
            .to("We're sold out. Please contact your account owner.")
        end
      end

      describe 'when download count is less than quota' do
        let(:quota) { 2 }
        let(:download_count) { 1 }
        let(:promo_code) { 'c848479' }

        before do
          allow(controller).to receive(:send_file).and_return(nil)
          get :show, show_params, valid_session
        end

        it 'updates download count on product' do
          expect(promo_code_object.download_count).to eq 2
        end

        it 'sends file' do
          expect(controller).to have_received(:send_file)
        end
      end
    end
  end
end
