require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'name' do
    it { should validate_presence_of(:name) }
  end

  describe 'file' do
    it { should have_attached_file(:file) }
  end

  describe 'promo code association' do
    it 'has many promo code' do
      should have_many(:promo_code)
        .dependent(:destroy)
    end
  end
end
