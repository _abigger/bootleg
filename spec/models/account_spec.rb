require 'rails_helper'

RSpec.describe Account, type: :model do
  describe 'name' do
    it { should validate_presence_of(:name) }
  end

  describe 'promo code association' do
    it { should have_many(:promo_code) }

    it 'accepts promo code attributes' do
      should accept_nested_attributes_for(:promo_code)
        .allow_destroy(true)
    end
  end
end
