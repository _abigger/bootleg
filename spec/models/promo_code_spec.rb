require 'rails_helper'

RSpec.describe PromoCode, type: :model do
  subject { described_class.new }

  describe 'quota' do
    it 'defaults to one' do
      expect(subject.quota).to eq 1
    end
  end

  describe 'download_count' do
    it 'defaults to zero' do
      expect(subject.download_count).to eq 0
    end
  end

  describe 'code' do
    context 'generating promo code' do
      let(:random_hex) { 'some-random-hex-last5' }

      before do
        allow(SecureRandom).to receive(:hex).and_return(random_hex)
      end

      it 'sets code to last 5 characters from secure random hex before save' do
        subject.save
        expect(subject.code).to eq 'last5'
      end
    end

    context 'persisting promo code' do
      before do
        @code = subject.code
      end

      it 'generates promo code once' do
        expect(subject.code).to eq @code
      end
    end
  end

  describe 'product association' do
    it 'has one product' do
      association = PromoCode.reflect_on_association(:product).macro
      expect(association).to eq :has_one
    end
  end
end
