AdminUser.create!(email: 'admin@bootleg.com', password: 'password', password_confirmation: 'password')

@melb = Product.create!(name: 'Melbourne')
@syd  = Product.create!(name: 'Sydney')

@unis = Account.create!(name: 'University of Somewhere')
@cc   = Account.create!(name: 'Community College')

PromoCode.create!(quota: 50, download_count: 25, product_id: @melb.id, account_id: @unis.id)
PromoCode.create!(quota: 10, download_count: 1, product_id: @syd.id, account_id: @cc.id)
PromoCode.create!(quota: 20, download_count: 2, product_id: @melb.id, account_id: @cc.id)
