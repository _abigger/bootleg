class AddFileToProducts < ActiveRecord::Migration
  def up
    add_attachment :products, :file
  end

  def down
    remove_attachment :products, :file
  end
end
