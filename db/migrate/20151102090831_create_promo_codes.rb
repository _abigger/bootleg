class CreatePromoCodes < ActiveRecord::Migration
  def change
    create_table :promo_codes do |t|
      t.integer :quota,           default: 1
      t.integer :download_count,  default: 0
      t.string :code
      t.belongs_to :product, index: true
      t.belongs_to :account, index: true

      t.timestamps null: false
    end
  end
end
